#!/usr/bin/perl
use strict;
use warnings;
foreach my $a(@ARGV)
{
	if (index($a,'-') >=0 && index($a,'-')<2)
	{
		print "$a is an option\n";
	}
	else
	{
		print "$a is a plain argument\n";
	}
}