#!/usr/bin/perl
use strict;
use warnings;

my @first = qw(Can unlock secret);
my @second = qw(you the code?);

my @mixed = interleave_words( scalar(@first), @first, @second );
print "Result: @mixed\n";

sub interleave_words
{
	my $firstarraylength = shift @_;
	my @firstarray = splice @_,0, $firstarraylength;
	my @secondarray = @_;
	my @results;
	my $count = scalar(@secondarray);	
	  if (scalar(@firstarray) != $count)
	  {
		die "Second array not same size ($count) as the first\n";
	  }
	  
  foreach my $index ( 0 .. $count-1 )
  {
    splice @results, $index*2, 0, $first[$index];
	splice @results, $index * 2 + 1,0,$second[$index];
  }
 
  return @results;
}

